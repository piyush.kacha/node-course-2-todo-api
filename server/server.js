require('./config/config');

let express = require('express');
let bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');

const {ObjectID} = require('mongodb');
const _ = require('lodash');

let {mongoose} = require('./db/mongoose');
let {Todo} = require('./models/todo');
let {User} = require('./models/user');
let {authenticate} = require('./middleware/authenticate');
let app = express();
app.use(bodyParser.json());

const port = process.env.PORT;

app.post('/todos',authenticate, (req, res) => {

    let todo = new Todo({
        text: req.body.text,
        _creator:req.user._id
    });

    todo.save()
        .then(data => {

            res.send(data);

        }).catch(err => {

        res.status(400).send(err);
    });

});

app.get('/todos',authenticate, (req, res) => {

    Todo.find({
        _creator:req.user._id
    })
        .populate('_creator')
        .then((todos) => {
        res.send({todos});

    }, (err) => {
        res.status(400).send(err);
    });

});

app.get('/todos/:_id',authenticate, (req, res) => {
    let id = req.params._id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Todo.findOne({
        _id:id,
        _creator:req.user._id
    }).then(data => {
        if (data) {
            res.send({todo: data});
        } else {
            res.status(404).send();
        }
    }).catch(err => {
        res.status(500).send({err: err});
    });

});


app.delete('/todos/:_id',authenticate, (req, res) => {
    let id = req.params._id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Todo.findOneAndRemove({

        _id:id,
        _creator:req.user._id

    }).then(data => {
        if (data) {
            res.send({todo: data});
        } else {
            res.status(404).send();
        }
    }).catch(err => {
        res.status(500).send({err: err});
    });

});

app.patch('/todos/:_id',authenticate, (req, res) => {

    console.log('Patch');
    let _id = req.params._id;
    let body = _.pick(req.body, ['text', 'completed']);

    if (!ObjectID.isValid(_id)) {
        return res.status(404).send();
    }

    if (_.isBoolean(body.completed) && body.completed) {

        body.completedAt = new Date().getTime();

    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findOneAndUpdate({
     _id:_id,
     _creator:req.user._id
    }, {$set: body}, {new: true})
        .then(todo => {
            if (todo) {
                res.send({todo: todo});
            } else {
                res.status(400).send();
            }
        }).catch(err => {

        res.status(400).send({error: err});
    });

});


app.post('/users/', (req, res) => {
    let body = _.pick(req.body, ['email', 'password']);
    let user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).send(user);
    }).catch((e) => {
        res.status(400).send(e);
    });


    // let body = _.pick(req.body, ['email', 'password', 'tokens']);
    //
    // let user = new User({
    //
    //     email: body.email,
    //     password: body.password,
    //     tokens: body.tokens
    // });
    //
    // user.save()
    //     .then((user1) => {
    //         const jwt = require('jsonwebtoken');
    //         let access = 'auth';
    //         let token = jwt.sign({_id: user._id.toHexString(), access}, 'abc123').toString();
    //         user1.tokens.push({access, token});
    //         return user1.save()
    //             .then((user3) => {
    //                 return token;
    //             })
    //     })
    //     .then((token) => {
    //         res.header('x-auth', token).send(user);
    //     }).catch(err => {
    //     res.status(400).send(err);
    // });

});


app.post('/users/login', (req, res) => {

    let body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password)
        .then(user => {

            return user.generateAuthToken().then((token) => {
                res.header('x-auth', token).send(user);
            });
        })
        .catch(err => {

            console.log("Error is:",err);
            res.status(500).send({"error":`${err}`});
        });


});


app.get('/users/me', authenticate, (req, res) => {

    res.send(req.user);
});

app.delete('/users/me/token',authenticate,(req,res)=>{

    req.user.removeToken(req.token).then(()=>{
       res.status(200).send();

    },()=>{
        res.status(400).send();
    });
});


app.listen(port, () => {
    console.log(`Started up at port ${port}`);
});

module.exports = {app};

